<HTML>
 <BODY>

<?php
function msgValuesArray($msgInput) {
   $pattern1 = '/R\$(\d){3},(\d){2}/'; //R$NNN,NN
   $pattern2 = '/R\$(\d){2}.(\d){3},(\d){2}/'; //R$NN.NNN,NN	
   $arrayResult = array(); 
   
   if(preg_match_all($pattern1, $msgInput, $result)) {
	  foreach($result as $resultado => $item ){
		  foreach($item as $cada => $valor ){
			 if($valor <> '0') 
             array_push( $arrayResult, $valor);
		  }
      }
   }
   if(preg_match_all($pattern2, $msgInput, $result)) {
	  foreach($result as $resultado => $item ){
		  foreach($item as $cada => $valor ){
			  if($valor <> '0') 
              array_push( $arrayResult, $valor);
		  }
      }
   }
   return $arrayResult;
}

print_r( msgValuesArray( "orçamento do projeto número 20/2021 será de R$20.000,20,
com incidência de R$500,60 de imposto de renda.") );
?>

 </BODY>
</HTML>

<?php
class Paciente{
	// Properties
	private $nome;
	
	//constructor
	function __construct($nome) {
		$this->nome = $nome;
	}  
}

class Avaliador{
	// Properties
	private $nome;
	
	//constructor
	function __construct($nome) {
		$this->nome = $nome;
	}  
}

class Pergunta{
	// Properties
	private $pergunta;

	//constructor
	function __construct($pergunta) {
		$this->pergunta = $pergunta;
	}
}

class Instrumento{
	// Properties
	private $instrumento;
	private $instrumentos = array();

	//constructor
	function __construct($instrumento) {
		$this->instrumento = $instrumento;
	}
	
	function adicionaPergunta($pergunta) {
		$ladd = 0;
		foreach(this->instrumentos as $instr => $perg ){
			if($perg == $pergunta) 
				$ladd = -1;
			else
				$ladd = 1;
		}
		if($ladd > 0 )
		   array_push($instrumentos, new Pergunta($pergunta) );
	    else
		return -1;
	}
}

class Avaliacao{
  // Properties
  private $paciente;
  private $avaliador;
  private $instrumento;
  private $pergunta = array();
  private $nota = array();

  //constructor
  function __construct($paciente, $avaliador, $instrumento) {
     $this->paciente    = $paciente;
	 $this->avaliador   = $avaliador;
	 $this->instrumento = new Instrumento($instrumento);
  }

  // Methods
  function atribuiEscore($pergunta, $nota) {
    $this->pergunta = $pergunta;
	$this->nota     = $nota;
	if( this->instrumento->adicionaPergunta($pergunta) < 0){
	   return -1;	
	}
	
  }
  
  function obtemTotalEscore() {
	$resultado = 0;  
	foreach(this->nota as $score => $valor ){
		$resultado += $valor;
	}  
    return $resultado;
  }
}
?>

<HTML>
 <BODY>
<?php
// Questão 1 - ler arquivo analise.csv e apresentar em uma tabela HTML
$separador = ';';
$vitaminaDpos = 0;
$tabelaHTML = '<table border="1">';
$tabelaValores = '<table border="1">';
$somaIdade = 0;
$somavitD = 0;
$somaCa = 0;
$contaH = 0;
$contaM = 0;
$contaLinha = 0;
$mediaIdade = 0.00;
$mediavitD = 0.00;
$mediaCa = 0.00;

$arq = fopen('d:/temp/analise.csv', 'r'); // Abrir arquivo para leitura
if ($arq) { 
    $cabecalho = fgetcsv($arq, 0, $separador); // Ler a linha de cabecalho do arquivo
	
	if(sizeOf($cabecalho) > 0) {
	  $tabelaHTML .= '<TR>';	
	}
	
	for ($i = 0; $i < sizeOf($cabecalho); $i++) {
       $tabelaHTML .= '<TH>' . ucfirst($cabecalho[$i]) . '</TH>';
	   if ($cabecalho[$i] == 'vitaminaD' ){
		   $vitaminaDpos = $i;
		   $tabelaHTML .= '<TH>Nível vitD</TH>';
	   }
    }

	if(sizeOf($cabecalho) > 0) {
	  $tabelaHTML .= '</TR>';	
	}
	
    while (!feof($arq)) { //enquanto houver registro será lido
        $linha = fgetcsv($arq, 0, $separador); // Lendo a linha do arquivo
        if (!$linha) {
            continue;
        }
	    $tabelaHTML .= '<TR>';
		for ($i = 0; $i < sizeOf($linha); $i++) {
		   $tabelaHTML .= '<TD align="center">' . $linha[$i] . '</TD>';
		   
		   if($i == $vitaminaDpos){
			   if( $linha[$i] > 49 ) {
			       $tabelaHTML .= '<TD align="center" style="color:green;">Suficiente</TD>';
			   }
			   else if($linha[$i] < 30 ) {
			       $tabelaHTML .= '<TD align="center" style="color:red;">Deficiente</TD>';
			   }
			   else {
				   $tabelaHTML .= '<TD align="center" style="color:orange;">Insuficiente</TD>';
			   }
		   }

		   switch($i){
			  case 0:
			     $somaIdade += $linha[$i];
			     break;
			  case 1:
			     $somavitD += (double) str_replace(',', '.', $linha[$i]);
			     break;
			  case 2:
			     $somaCa += (double) str_replace(',', '.', $linha[$i]);
			     break;
			  case 3:
			     if ($linha[$i] == "H") $contaH ++; else $contaM ++;
			     break;				 
		   }
		}
	    $contaLinha ++;
		$tabelaHTML .= '</TR>';
	}
    fclose($arq);

	$mediaIdade = $somaIdade / $contaLinha;
    $mediavitD  = $somavitD / $contaLinha ;
    $mediaCa    = $somaCa / $contaLinha;

	$tabelaValores .= '<TR><TH>Média Idade</TH><TH>Média vitD</TH><TH>Média Ca</TH><TH># H</TH><TH># M</TH></TR>';
	$tabelaValores .= '<TR><TD align="center">' . number_format($mediaIdade, 2, ',', '') . '</TD><TD align="center">' . number_format($mediavitD, 2, ',', '') . '</TD><TD align="center">' . number_format($mediaCa, 2, ',', '') . '</TD><TD align="center">' . $contaH . '</TD><TD align="center">' . $contaM  . '</TD></TR>';
}
$tabelaHTML .= '</table>';
echo $tabelaHTML; //Apresentando o conteúdo do arquivo numa tabela HTML
echo '<BR>';
echo $tabelaValores; //Apresentando a tabela de Médias e valores
?>

 </BODY>
</HTML>
